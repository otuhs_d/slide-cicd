### Markdown で記述

お手軽・簡単

---
### reveal-ck でスライドに

コマンド1つでOK

```
$ reveal-ck generate
```

---
### GitLab でバージョン管理

さようなら

* xxx\_最新版.pptx
* レビュアごとに違う指摘

---
### GitLab Pages にホスト

手軽に公開・共有

---
### GitLab CI で CI/CD

push されるたびに自動で Pages も更新
